"   vim: set foldmarker={,} foldlevel=5 spell:

call pathogen#runtime_append_all_bundles()
call pathogen#helptags()

let g:syntastic_auto_loc_list=1
let g:syntastic_auto_jump=1
let g:syntastic_enable_signs=1

" Basics {

     set nocompatible    " explicitly get out of vi-compatible mode
     set noexrc          " don't use local version of .(g)vimrc, .exrc
     set background=dark " we plan to use a dark background
     set magic           " enable magic regex

     set cpoptions=aABceFsmq
     "             |||||||||
     "             ||||||||+-- When joining lines, leave the cursor between joined lines
     "             |||||||+-- When a new match is created (showmatch) pause for .5
     "             ||||||+-- Set buffer options when entering the buffer
     "             |||||+-- :write command updates current file name
     "             ||||+-- Automatically add <CR> to the last line when using :@r
     "             |||+-- Searching continues at the end of the match at the cursor position
     "             ||+-- A backslash has no special meaning in mappings
     "             |+-- :write updates alternative file name
     "             +-- :read updates alternative file name

     syntax on " syntax highlighting on

     let g:snips_author = "Oliver O'Neill <oliver@travelfusion.com>"
 " }
 
 " General {

     filetype plugin indent on      " load filetype plugins/indent settings
     set autochdir                  " always switch to the current file directory
     set backspace=indent,eol,start " make backspace a more flexible
     set backup                     " make backup files
     set backupdir=~/.vim/backup    " where to put backup files
     set clipboard+=unnamed         " share windows clipboard
     set directory=~/.vim/tmp       " directory to place swap files in
     set fileformats=unix,dos,mac   " support all three, in this order
     set hidden                     " you can change buffers without saving
     set tags=./tags;$HOME

     " (XXX: #VIM/tpope warns the line below could break things)
     set iskeyword+=_,$,@,%,# " none of these are word dividers
     "set mouse=a " use mouse everywhere
     set noerrorbells " don't make noise
     set whichwrap=b,s,h,l,<,>,~,[,] " everything wraps
     "             | | | | | | | | |
     "             | | | | | | | | +-- "]" Insert and Replace
     "             | | | | | | | +-- "[" Insert and Replace
     "             | | | | | | +-- "~" Normal
     "             | | | | | +-- <Right> Normal and Visual
     "             | | | | +-- <Left> Normal and Visual
     "             | | | +-- "l" Normal and Visual (not recommended)
     "             | | +-- "h" Normal and Visual (not recommended)
     "             | +-- <Space> Normal and Visual
     "             +-- <BS> Normal and Visual
     set wildmenu " turn on command line completion wild style

     " ignore these list file extensions
     set wildignore=*.dll,*.o,*.obj,*.bak,*.exe,*.pyc,*.jpg,*.gif,*.png
     set wildmode=list:longest " turn on wild mode huge list

     " The completion dictionary is provided by Rasmus:
     " http://lerdorf.com/funclist.txt
     set dictionary-=~/.vim/funclist.txt dictionary+=~/.vim/funclist.txt

     " Use the dictionary completion
     set complete-=k complete+=k
     set encoding=utf-8
     set gdefault

 " }
 
 " Vim UI {

    set cursorcolumn             " highlight the current column
    set cursorline               " highlight current line
    set incsearch                " BUT do highlight as you type you search phrase
    set laststatus=2             " always show the status line
    set lazyredraw               " do not redraw while running macros
    set linespace=0              " don't insert any extra pixel lines betweens rows
    set list                     " we do what to show tabs, to ensure we get them out of my files
    set listchars=tab:>-,trail:- " show tabs and trailing
    set matchtime=5              " how many tenths of a second to blink matching brackets for
    set hlsearch                 " highlight searched for phrases
    set nostartofline            " leave my cursor where it was
    set novisualbell             " don't blink
    set number                   " turn on line numbers
    set numberwidth=5            " We are good up to 99999 lines
    set report=0                 " tell us when anything is changed via :...
    set ruler                    " Always show current positions along the bottom
    set scrolloff=10             " Keep 10 lines (top/bottom) for scope
    set shortmess=aOstT          " shortens messages to avoid 'press a key' prompt
    set showcmd                  " show the command being typed
    set showmatch                " show matching brackets
    set sidescrolloff=10         " Keep 5 lines at the size
    set statusline=%F%m%r%h%w[%L][%{&ff}]%y[%p%%][%04l,%04v]%{fugitive#statusline()}%{SyntasticStatuslineFlag()}
    "              | | | | |  |   |      |  |     |    |      |
    "              | | | | |  |   |      |  |     |    |      |
    "              | | | | |  |   |      |  |     |    |      +--------- Git Status
    "              | | | | |  |   |      |  |     |    + current column
    "              | | | | |  |   |      |  |     +-- current line
    "              | | | | |  |   |      |  +-- current % into file
    "              | | | | |  |   |      +-- current syntax in square brackets
    "              | | | | |  |   +-- current fileformat
    "              | | | | |  +-- number of lines
    "              | | | | +-- preview flag in square brackets
    "              | | | +-- help flag in square brackets
    "              | | +-- readonly flag in square brackets
    "              | +-- modified flag in square brackets
    "              +-- full path to file in the buffer

    set colorcolumn=80

" }

" Text Formatting/Layout {
    "set completeopt=longest,menuone " don't use a pop up menu for completions

    set expandtab          " no real tabs please!
    set formatoptions=qrn1 " Automatically insert comment leader on return, and let gq format comments
    set ignorecase         " case insensitive by default
    set infercase          " case inferred by default
    set nowrap             " do not wrap line
    set shiftround         " when at 3 spaces, and I hit > ... go to 4, not 5
    set smartcase          " if there are caps, go case-sensitive
    set shiftwidth=4       " auto-indent amount when using cindent, >>, << and stuff like that
    set softtabstop=4      " when hitting tab or backspace, how many spaces should a tab be (see expandtab)
    set tabstop=4          " real tabs should be 8, and they will show with set list on

" }

" Folding {

    set foldenable                                   " Turn on folding
    set foldmarker={,}                               " Fold C style code (only use this as default if you use a high foldlevel)
    set foldmethod=marker                            " Fold on the marker
    set foldlevel=1000                               " Don't autofold anything (but I can still fold manually)
    set foldopen=block,hor,mark,percent,quickfix,tag " what movements open folds

" }

" Plugin Settings {
    " TagList Settings {

        let Tlist_Auto_Open            = 1       " let the tag list open automagically
        let Tlist_Compact_Format       = 1       " show small menu
        let Tlist_Ctags_Cmd            = 'ctags' " location of ctags
        let Tlist_Enable_Fold_Column   = 0       " do show folding tree
        let Tlist_Exist_OnlyWindow     = 1       " if you are the last, kill yourself
        let Tlist_File_Fold_Auto_Close = 0       " fold closed other trees
        let Tlist_Sort_Type            = 'name'  " order by
        let Tlist_Use_Right_Window     = 1       " split to the right side of the screen
        let Tlist_WinWidth             = 40      " 40 cols wide, so i can (almost always) read my functions

        let Tlist_Exit_OnlyWindow = 1

        " Language Specifics {
            " just functions and classes please
            let tlist_aspjscript_settings = 'asp;f:function;c:class' 
            " just functions and subs please
            let tlist_aspvbs_settings = 'asp;f:function;s:sub' 
            " don't show variables in freaking php
            let tlist_php_settings = 'php;c:class;d:constant;f:function' 
            " just functions and classes please
            let tlist_vb_settings = 'asp;f:function;c:class' 
        " }
    " }
    " File Cache {
        let g:FindFileIgnore = ['*.o', '*.pyc', '*/tmp/*', '*/cache/*', '*/.svn/*', '*.txt','*.log','*.zip','*.sql','*.*~','*.html']
    " }

    filetype plugin on
    au FileType php set omnifunc=phpcomplete#CompletePHP

    let php_sql_query=1
    let php_htmlInStrings=1
    let PHP_vintage_case_default_indent=1

" }

" Mappings {
if has("autocmd")
" F5, F6 maaping
" imap allow recursive mapping, [i] noremap NOT allow
"
    autocmd FileType php    noremap <F5> :!php-cgi %<CR>
    autocmd FileType php    noremap <F6> :!php -l %<CR>
    autocmd FileType php    noremap <F7> :Phpcs<CR>
    autocmd FileType java   noremap <F5> :!java %:r<CR>
    autocmd FileType python noremap <F5> :!python %<CR>
    autocmd FileType python noremap <F7> :!pythonw %<CR>
    autocmd FileType tcl    noremap <F5> :!ns2 %<CR>
    autocmd FileType c,cpp  noremap <F5> :!gcc -c -Wall %<CR>
    autocmd FileType ruby   noremap <F5> :!ruby %<CR>
    autocmd FileType perl   noremap <F5> :!perl %<CR>
    autocmd FileType php    set omnifunc=phpcomplete#CompletePHP

    " different format options
    autocmd FileType *  noremap <F8> :Tab<CR>
    autocmd FileType *  set nocindent
    autocmd FileType *  set nosmartindent
    "autocmd FileType c,cpp,php  set cindent

    " Comment Block of Text
    " Python/Perl # comments
    autocmd FileType python,perl,makefile    map ,r :s/^/# /<CR>
    autocmd FileType python,perl,makefile    map ,t :s/^# \=//<CR>

    " C/C++/C#/Java // comments
    autocmd FileType c,cpp,java     map ,r :s/^/\/\/ /<CR>
    autocmd FileType c,cpp,java     map ,t :s/^\/\/ \=//<CR>
    autocmd FileType javascript,php     map ,r :s/^/\/\/ /<CR>
    autocmd FileType javascript,php     map ,t :s/^\/\/ \=//<CR>

    filetype indent on
endif

map <F9> <ESC>:TlistToggle<CR>
map <C-\> :tab split<CR>:exec("tag ".expand("<cword>"))<CR>
map <A-]> :vsp <CR>:exec("tag ".expand("<cword>"))<CR>

" }

" GUI Settings {
if has("gui_running")
    " Basics {
        colorscheme darkspectrum
        set columns=180 " perfect size for me
        set guifont=Liberation\ Mono\ 10
        set guioptions=em
        "              |
        "              +-- use simple dialogs rather than pop-ups
        set lines=55 " perfect size for me
        set mousehide " hide the mouse cursor when typing
    " }

endif
" }
